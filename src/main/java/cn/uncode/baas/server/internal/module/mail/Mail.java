/**
 * Copyright@xiaocong.tv 2012
 */
package cn.uncode.baas.server.internal.module.mail;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;

/**
 * @author caijun.du
 */
public class Mail {

    private String mailServerHost;// 发送邮件的服务器ip
    private String mailServerPort;// 发送邮件的服务器端口
    private String fromAddress;// 发送者的邮件地址
    private List<String> toAddress;// 邮件接收者地址
    private String username;// 登录邮件发送服务器的用户名
    private String password;// 登录邮件发送服务器的密码
    private boolean validate = true;// 是否需要身份验证
    private String subject;// 邮件主题
    private String content;// 邮件内容
    private int type;
    private String[] attachFileNames;// 附件名称

    public Properties getProperties() {
        Properties p = new Properties();
        p.put("mail.smtp.host", this.mailServerHost);
        p.put("mail.smtp.port", this.mailServerPort);
        p.put("mail.smtp.auth", validate ? "true" : "false");
        return p;
    }

    public String getMailServerHost() {
        return mailServerHost;
    }

    public void setMailServerHost(String mailServerHost) {
        this.mailServerHost = mailServerHost;
    }

    public String getMailServerPort() {
        return mailServerPort;
    }

    public void setMailServerPort(String mailServerPort) {
        this.mailServerPort = mailServerPort;
    }

    public String getFromAddress() {
        return fromAddress;
    }

    public void setFromAddress(String fromAddress) {
        this.fromAddress = fromAddress;
    }

    public List<String> getToAddress() {
        return toAddress;
    }

    public void setToAddress(List<String> toAddress) {
        this.toAddress = toAddress;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isValidate() {
        return validate;
    }

    public void setValidate(boolean validate) {
        this.validate = validate;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String[] getAttachFileNames() {
        return attachFileNames;
    }

    public void setAttachFileNames(String[] attachFileNames) {
        this.attachFileNames = attachFileNames;
    }

    public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public static Mail valueOf(Map<String, Object> map){
    	Mail mail = new Mail();
    	if(map.containsKey("service")){
    		mail.setMailServerHost(String.valueOf(map.get("service")));
    	}else{
    		mail.setType(3);
    	}
    	if(map.containsKey("port")){
    		mail.setMailServerPort(String.valueOf(map.get("port")));
    	}else{
    		mail.setType(3);
    	}
    	mail.setValidate(true);
    	if(map.containsKey("auth")){
    		Map<?,?> authMap = (Map<?, ?>) map.get("auth");
    		if(null != authMap){
    			if(authMap.containsKey("user")){
    				mail.setUsername(String.valueOf(authMap.get("user")));
    			}
    			if(authMap.containsKey("pass")){
    				mail.setPassword(String.valueOf(authMap.get("pass")));
    			}
    		}
    	}else{
    		mail.setType(3);
    	}
    	if(map.containsKey("from")){
    		mail.setFromAddress(String.valueOf(map.get("from")));
    	}else{
    		mail.setType(3);
    	}
    	if(map.containsKey("to")){
    		String toStr = String.valueOf(map.get("to"));
    		if(StringUtils.isNotBlank(toStr)){
    			mail.setToAddress(Arrays.asList(toStr.split(",")));
    		}
    	}else{
    		mail.setType(3);
    	}
    	if(map.containsKey("subject")){
    		mail.setSubject(String.valueOf(map.get("subject")));
    	}else{
    		mail.setType(3);
    	}
    	if(map.containsKey("text")){
    		mail.setContent(String.valueOf(map.get("text")));
    		mail.setType(1);
    	}else if(map.containsKey("html")){
    		mail.setContent(String.valueOf(map.get("html")));
    		mail.setType(2);
    	}else{
    		mail.setType(3);
    	}
    	mail.setValidate(true);
    	return mail;
    }
    	
    

}
